import java.util.Scanner;

public class Lab1_2_DuplicateZero {
    public static void doubleZero(int arr[]) {
        for(int i=0;i<arr.length;i++){
            if(arr[i] == 0){
                for(int j=arr.length-1;j>i;j--){
                    arr[j] =arr[j-1];
                }
                i++;
            }
        }
    }

    public static void doubleZero1(int input[]){

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int[] arr = new int[length];
        for(int i=0;i<arr.length;i++){
            arr[i] = sc.nextInt();
        }
        doubleZero(arr);

        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }

    }
}
