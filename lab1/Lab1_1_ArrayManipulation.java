import java.util.Scanner;

public class Lab1_1_ArrayManipulation {
    public static void main(String[] args) {
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        Double[] values = {0.0,0.0,0.0,0.0};

        for(int i=0;i<numbers.length;i++){
            System.out.print(numbers[i]+" ");
        }
        System.out.println();

        for(int i=0;i<names.length;i++){
            System.out.print(names[i]+" ");
        }
        System.out.println();

        for(int i=0;i<values.length;i++){
            Scanner sc = new Scanner(System.in);
            values[i] = sc.nextDouble();
        }

        for(int i=0;i<values.length;i++){
            System.out.print(values[i]+" ");
        }

        System.out.println();

        int sum = 0;
        for(int i=0;i<numbers.length;i++){
            sum = numbers[i] + sum;
        }
        System.out.println(sum);

        Double max = 0.0;
        for(int i=0;i<values.length;i++){
            if(values[i] > max){
                max = values[i];
            }
        }

        System.out.println(max);

        String[] reversedNames = {" "," "," "," "};
        for(int i=0;i<reversedNames.length;i++){
            reversedNames[i] = names[3-i];
            System.out.print(reversedNames[i]+" ");
        }

        for(int i=0;i<numbers.length-1;i++){
            for(int j=0;j<numbers.length-1;j++){
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }

        System.out.println();
        for(int i=0;i<numbers.length;i++){
            System.out.print(numbers[i]+" ");
        }
    }
}